<?php

$query = array();

$days = array(
	0 =>'monday',
	1 =>'tuesday',
	2 =>'wednesday',
	3 =>'thurstday',
	4 =>'friday',
	5 =>'saturday',
	6 =>'sunday'
);

// temporary for London event
$query['branchId'] = 2;
	
$branchId = $query['branchId'];
$colorSchemaType = (!empty($_GET['colorSchema']) && in_array($_GET['colorSchema'], array('weekday', 'hour'))) ? (string)$_GET['colorSchema'] : 'hour';
$day = (!empty($_GET['day']) && array_key_exists((int)$_GET['day'], $days) ? (int)$_GET['day'] : 0);

if (!empty($_GET['username']) && preg_match('/[a-zA-Z0-9_.]+$/',$_GET['username'])) {
	$query['username'] = (string)$_GET['username'];
	$jsonUrl = "geoJson.php";
	$jsonUrl .= (count($query)) ? "?".http_build_query($query) : '';
} else {
	$jsonUrl = "geojson/" . $days[$day] . ".json";
}
?><!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script>
		var infoWindow = new google.maps.InfoWindow({
			content: ""
		});
		function initialize() {
			var mapProp = {
				center:new google.maps.LatLng( 51.5309, -0.1233),
				zoom:13,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			};

			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			map.data.setStyle(styleFeature);

			map.data.loadGeoJson("<?php echo $jsonUrl; ?>");

			map.data.addListener('click', function(event) {


				event.feature.getProperty("postThumb");
				//show an infowindow on click
				var content = "";

				var leftBox = "<div style='display: inline-block'>";
				leftBox = '<img src="' + event.feature.getProperty("postThumb") + '" alt="post" />';
				leftBox+= "</div>";

				content = leftBox; // + rightBox;
				infoWindow.setContent(content);


				var anchor = new google.maps.MVCObject();
				anchor.set("position",event.latLng);
				infoWindow.open(map,anchor);
				$('.metaInfo').html(event.feature.getProperty("html"));
			});

			function interpolateHsl(lowHsl, highHsl, fraction) {
				var color = [];
				for (var i = 0; i < 3; i++) {
					// Calculate color based on the fraction.
					color[i] = (highHsl[i] - lowHsl[i]) * fraction + lowHsl[i];
				}

				return 'hsl(' + color[0] + ',' + color[1] + '%,' + color[2] + '%)';
			}
			function styleFeature(feature) {
				var colorSchemType = '<?php echo $colorSchemaType;?>';
				var low = [151, 83, 34];
				var high = [5, 69, 54];
				var minVal = 0.0;
				if (colorSchemType == 'weekday') {
					var maxVal = 7.0;
				} else if (colorSchemType == 'hour') {
					var maxVal = 24.0;
				}

				// fraction represents where the value sits between the min and max
				var fraction = (Math.min(feature.getProperty(colorSchemType), maxVal) - minVal) /
					(maxVal - minVal);

				var color = interpolateHsl(low, high, fraction);

				return {
					icon: {
						path: google.maps.SymbolPath.CIRCLE,
						strokeWeight: 0.5,
						strokeColor: '#fff',
						fillColor: color,
						fillOpacity: 1.0,
						scale: 4.0
					},
					zIndex: Math.floor(feature.getProperty(colorSchemType))
				};
			}
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<style type="text/css">
		.metaInfo {
			padding: 10px;
			border: 1px solid gray;
			background-color: #ededed;
			position: absolute;
			width: 400px;
			top: 10px;
			bottom: 10px;
			right: 10px;
		}

		label {
			display: block;
			font-weight: bold;
			margin-top: 10px;
		}

		.fullPic {
			position: absolute;
			background-color: #ffF;
			padding: 10px;
			top: 10px;
			left: 10px;

		}

		.fullPic.hidden {
			display: none;
		}
	</style>
</head>
<body>
<nav>
	<a href="trunk.php?day=0">Mon</a>
	<a href="trunk.php?day=1">Tue</a>
	<a href="trunk.php?day=2">Wed</a>
	<a href="trunk.php?day=3">Thu</a>
	<a href="trunk.php?day=4">Fri</a>
	<a href="trunk.php?day=5">Sat</a>
	<a href="trunk.php?day=6">Sun</a>
</nav>
<div>
	<div id="googleMap" style="width:700px;height:700px;"></div>
</div>
<div class="metaInfo"></div>
<div class="fullPic hidden"><img src="" /></div>

<script type="text/javascript">
	var fullPic = function(picUrl) {
		var fullSizeImg = $('.fullPic');
		$('.fullPic img').attr('src', picUrl);
		fullSizeImg.removeClass('hidden');

	};

	$('.fullPic').on('click', function() {
		$(this).addClass('hidden');
	});
</script>
</body>
</html>